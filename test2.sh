#!/bin/bash

:<<YAY
source util.sh

use_dir "out"

write_motd "Pentateuco ad aternvm" "cola"


function test_it {
  [[ -z "$1" ]] && return 1 || return 0
}

test_it
echo $?

test_it ''
echo $?

test_it true
echo $?
YAY

#printf "\n" > foo.txt
#printf "\n" >> foo.txt

some=1
thing="wow"

[[ $some -gt 0 ]] && [[ -z "$thing" ]] && echo "alright!"
