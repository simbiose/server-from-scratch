#!/bin/bash

USER="$1"
HOSTGROUP="$2"

# check group exists
/bin/egrep  -i "^${HOSTGROUP}" /etc/group
if [ ! $? -eq 0 ]; then
  groupadd $HOSTGROUP
fi

# check user exists
/bin/egrep  -i "^${USER}" /etc/passwd
if [ $? -eq 0 ]; then
  usermod -G$HOSTGROUP $USER
else
  if [ $3 -eq 1 ]; then
    # system user
    useradd --system --no-create-home -g $HOSTGROUP $USER
  else
    useradd -g $HOSTGROUP $USER

    # ftp - http
    mkdir /home/$USER
    mkdir /home/$USER/www
    touch /home/$USER/www/index.html
    cat > /home/$USER/www/index.html <<"EOF"
    <!doctype html>
    <html>
      <head>
        <title>${USER} home page</title>
      </head>
      <body>
        <br /><br />
        <center>
          <h2>is it future ${USER}'s home page?</h2>
        </center>
      </body>
    </html>
EOF

    # nginx http virtual host
    mkdir /home/$USER/log
    touch /home/$USER/log/vhost.nginx.log
    mkdir /home/$USER/conf
    touch /home/$USER/conf/vhost.nginx.conf

    # basic conf
    cat > /home/$USER/conf/vhost.nginx.conf <<"EOF"
    server{
      listen       80;
      server_name  ${USER}.simbio.se;
      root         /home/${USER}/www;
      charset      utf-8;
      access_log   /home/${USER}/log/vhost.nginx.log vhost;

      location / { index index.html index.htm; }

      error_page 500 502 503 504 /50x.html;
      location = /50x.html { root html; }
    }
EOF

    ln -fs /home/$USER/conf/vhost.nginx.conf /etc/nginx/sites-enabled/$USER.nginx.conf

    # ssh authorization
    mkdir /home/$USER/.ssh
    mkdir /home/$USER/.ssh/authorized_keys

    # authorization and rights
    chmod 755 -R /home/$USER
    chown -R $USER:$HOSTGROUP /home/$USER

    exit 0
  fi
fi
