#! /bin/bash
#
# "THE BEER-WARE LICENSE" (Revision 42):
# <xxleite@gmail.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return
#

# bash 4
if ((BASH_VERSINFO[0] < 4)); then echo "Sorry, you need at least bash-4.0 to run this script." >&2; exit 1; fi

match_comment="^[;#]"
match_section="^\[(.*)\]"
match_key_value="^\s*([^=]*)=\s*(.*);*\s*$"
regex_filename_ext=".+(/(.*)\.(tar.gz|tgz|tar.bz2|zip|flf|txt|csv)\??(.*))"
cur_dir=$(pwd)
base_dir=$cur_dir/
declare -A ini_key_value
declare -a ini_section_order

#
function err { 
  echo "$(tput bold)$(tput setaf 1)$1$(tput sgr0)" 
}

#
function puts { 
  echo "$(tput setaf 4)$1$(tput sgr0)" 
}

# assert last command
# $1 message
# $2 on error execute this
# $3 true for failsafe
function assert {
  if [ $? -gt 0 ]; then
    err $1
    [[ ! -z "$2" ]] && eval "$3"
    [[ -z "$3" ]] && exit 1
  fi
}

# defines base_dir
# $1 dir name
function use_dir {
  base_dir=$(pwd)/
  [[ "$1" != "." ]] && base_dir=$base_dir$1/
  mkdir -p "$base_dir"tmp
  mkdir -p "$base_dir"tarballs
  mkdir -p "$base_dir"libs
  mkdir -p "$base_dir"output
}

# move folders (and files) from tmp folder
# $1 final directory name relative to base_dir
# $2 alternative directory name
function move_folder {
  if [ -z "$2" ]; then
    local final_dir="$base_dir"libs/$1
  else
    local final_dir=$2
  fi
  mkdir -p $final_dir
  cd "$base_dir"tmp
  for dir in *
  do
    if [ ! -f $dir ]; then
      cp -R $dir/* $final_dir
      rm $dir -r -f
    else
      cp $dir $final_dir
      rm $dir -r -f
    fi
  done
  cd "$cur_dir"
}

# downloads and decompress tar,bz2 and zip
# $1=source 
# $2=base folder 
# $3=true for "failsafe" | null for abort in error
function download {

  if [[ $1 =~ $regex_filename_ext ]]; then
    folder="${BASH_REMATCH[2]}"
    ext="${BASH_REMATCH[3]}"

    if [ -f "$base_dir"tarballs/"$folder.$ext" ]; then
      puts ""
      puts " ... file '$folder.$ext' already downloaded ... "
    else
      puts ""
      puts " ... downloading $folder.$ext ... "
      curl --output "$base_dir"tarballs/"$folder.$ext" $1 --progress-bar -L
    fi

    assert " download fail " "rm -f ${base_dir}tarballs/$folder.$ext" $3
    [[ ! -z "$3" ]] && [[ $? -gt 0 ]] && return 1

    #if [ $? -gt 0 ]; then
    #  err " download fail "
    #  rm -f "$base_dir"tarballs/"$folder.$ext"
    #  [[ -z "$3" ]] && exit 1 || return 1
    #fi

    # if folder does not exists
    if [[ ! -d "$base_dir"libs/"$folder" ]] ; then 
      if [ "$ext" == "tar.gz" ] || [ "$ext" == "tgz" ]; then
        tar zxf "$base_dir"tarballs/"$folder.$ext" -C "$base_dir"tmp
        move_folder "$folder" "$2"
      elif [ "$ext" == "tar.bz2" ]; then
        tar jxf "$base_dir"tarballs/"$folder.$ext" -C "$base_dir"tmp
        move_folder "$folder" "$2"
      elif [ "$ext" == "zip" ]; then
        unzip -d "$base_dir"tmp "$base_dir"tarballs/"$folder.$ext"
        move_folder "$folder" "$2"
      else
        puts "its not a common compressed format $ext"
        mv "$base_dir"tarballs/"$folder.$ext" "$base_dir"tmp
        move_folder "$folder.$ext" "$2"
      fi
    else
      puts " ... folder '$folder' already decompressed ... "
    fi
    
    puts ""
    assert "decompression fail" "" "$3"
    [[ ! -z "$3" ]] && [[ $? -gt 0 ]] && return 1
    puts "$folder"

    #if [ $? -gt 0 ]; then
    #  puts ""
    #  err "decompression fail"
    #  [[ -z "$3" ]] && exit 1 || return 1
    #else
    #  puts ""
    #  echo "$folder"
    #fi
  fi
}

# write word(s) to motd using figlet fonts
# $1 word(2)
# $2 font name
function write_motd {
  # try figlet otherwise abort
  command -v figlet >/dev/null 2>&1 || { err "Figlet not found. Aborting." >&2; exit 1; }
  # try to download font
  download "http://www.jave.de/figlet/fonts/details/$2.flf" "$base_dir"fonts true
  # write to motd anyway
  printf "\n" > /etc/motd.tail
  [[ $? -gt 0 ]] && figlet "$1" >> /etc/motd.tail || figlet -f "$base_dir"fonts/"$2.flf" "$1" >> /etc/motd.tail
  printf "\n" >> /etc/motd.tail 
}

# process a source file ... downloads, configure (autotools) and installs (make, cmake)
# $1 first command (env variables and etc)
# $2 source (http repo and etc)
# $3 configuring command
# $4 compile command
function process {

  local src=$2
  local cmd=$3
  local cmp=$4

  if [[ $src =~ $regex_filename_ext ]]; then
      
    local folder="${BASH_REMATCH[2]}"
    local ext="${BASH_REMATCH[3]}"

    download "$src"

    if [[ $folder =~ $sanitize_folder_name ]]; then
      local sanitized="${BASH_REMATCH[1]}"
      eval "export ${sanitized}_src=${base_dir}libs/${folder}"
    fi
    
    cd "$base_dir"libs/"$folder"

    eval $1

    assert "fail to configure" "" true

    if [ -f ./config ]; then
      if [ "$cmd" == "" ]; then
        puts ""
        puts "configuring ..."
        eval "./config > /dev/null"
      else
        puts ""
        puts "configuring with $cmd ..."
        eval "./config $cmd > /dev/null"
      fi
    elif [ -f ./configure ]; then
      if [ "$cmd" == "" ]; then
        puts ""
        puts "configuring ...."
        eval "./configure > /dev/null"
      else
        puts ""
        puts "configuring with $cmd ...."
        eval "./configure $cmd > /dev/null"
      fi
    else
      if [ "$cmd" != "" ]; then
        puts ""
        puts "executing command $cmd .."
        eval "$cmd > /dev/null"
      fi
    fi

    assert "configuration fail" "cd $cur_dir" 

    #if [ $? -gt 0 ]; then
    #  puts ""
    #  err "configuration fail"
    #  cd "$cur_dir"
    #  exit 1
    #fi

    if [ -f ./Make ] || [ -f ./make ] || [ -f ./Makefile ] || [ -f ./makefile ]; then
      if [ -z "$cmp" ]; then
        puts "building ..."
        make > /dev/null 
        puts "installing ..."
        make install > /dev/null
        puts "cleaning ..."
        make clean > /dev/null
      else
        puts "building with arguments: $cmp .."
        eval "$cmp"
      fi
    else
      if [ -f ./cmake ] || [ -d ./cmake ] || [ -f ./CmakeLists.txt ]; then
        if [ -z "$cmp" ]; then
          puts "configuring cmake ..."
          eval "cmake $cmd > /dev/null"
          puts "compiling ..."
          make > /dev/null
          puts "installing ..."
          make install > /dev/null
        else
          puts "building with arguments: $cmp ...."
          eval "$cmp"  
        fi
      else
        err "could not find build files"
        exit 1
      fi
    fi

    assert "build fail" "cd $cur_dir"

    #if [ $? -gt 0 ]; then
    #  err "configuration fail"
    #  cd "$cur_dir"
    #  exit 1
    #fi
      
    cd "$cur_dir"

  else
    err "could not download $src"
    exit 1
  fi
}

# add a user, create default subdomain and page
# $1 user name
# $2 group name
# $3 true for subdomain & default page creation
function user_add {
  local USER="$1"
  local HOSTGROUP="$2"

  # check group exists
  /bin/egrep  -i "^${HOSTGROUP}" /etc/group
  if [ ! $? -eq 0 ]; then
    groupadd $HOSTGROUP
  fi

  # check user exists
  /bin/egrep  -i "^${USER}" /etc/passwd
  if [ $? -eq 0 ]; then
    usermod -G$HOSTGROUP $USER
  else
    if [ -z "$3" ]; then
      # system user
      useradd --system --no-create-home -g $HOSTGROUP $USER
    else
      useradd -g $HOSTGROUP $USER

      # ftp - http
      mkdir /home/$USER
      mkdir /home/$USER/www
      touch /home/$USER/www/index.html
      cat > /home/$USER/www/index.html <<"EOF"
      <!doctype html>
      <html>
        <head>
          <title>${USER} home page</title>
        </head>
        <body>
          <br /><br />
          <center>
            <h2>is it future ${USER}'s home page?</h2>
          </center>
        </body>
      </html>
EOF

      # nginx http virtual host
      mkdir /home/$USER/log
      touch /home/$USER/log/vhost.nginx.log
      mkdir /home/$USER/conf
      touch /home/$USER/conf/vhost.nginx.conf

      # basic conf
      cat > /home/$USER/conf/vhost.nginx.conf <<"EOF"
      server{
        listen       80;
        server_name  ${USER}.simbio.se;
        root         /home/${USER}/www;
        charset      utf-8;
        access_log   /home/${USER}/log/vhost.nginx.log vhost;

        location / { index index.html index.htm; }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html { root html; }
      }
EOF
      # 
      mkdir -p /etc/nginx/sites-enabled
      ln -fs /home/$USER/conf/vhost.nginx.conf /etc/nginx/sites-enabled/$USER.nginx.conf

      # ssh authorization
      mkdir /home/$USER/.ssh
      mkdir /home/$USER/.ssh/authorized_keys

      # authorization and rights
      chmod 755 -R /home/$USER
      chown -R $USER:$HOSTGROUP /home/$USER

      return 0
    fi
  fi
}

# read/parse ini files
# $1 path to file
function ini.parse {

  [[ ! -f $1 ]] && return 1

  local order=0
  ini_section_order[0]="_"
  ((order++))

  while read line; do
    [[ $line =~ $match_comment ]] && continue
    if [[ $line =~ $match_section ]]; then
      section="${BASH_REMATCH[1]//[[:space:]]/}"
      ini_section_order[$order]="$section"
      ((order++))
    elif [[ $line =~ $match_key_value ]]; then
      ini_key_value["${section}_${BASH_REMATCH[1]//[[:space:]]/}"]="${BASH_REMATCH[2]}"
    fi
  done < $1
  return 0
}

# add key/value (section) to ini
# $1 key
# $2 value
# $3 section name
function ini.add {
  [[ -z "$1" ]] && return 1
  local section=$3
}

# delete key (section) of ini
# $1 key
# $2 section
function ini.del {
  return 1
}

# save options to ini file
# $1 path to file
function ini.save {

  local utime section key
  utime=$(date +%s)

  [[ -f "$1" ]] && eval "\cp $1 $1.old.$utime" || touch $1

  for section in "${ini_section_order[@]}"; do
    [[ "$section" == "_" ]] && echo -e "[]" > "$1" || echo -e "\n[$section]" >> "$1"
    for key in "${!ini_key_value[@]}"; do
      [[ "$key" == "${section}_"* ]] && echo -e "${key#${section}_} = ${ini_key_value[$key]}" >> "$1"
    done
  done

  unset ini_section_order
  unset ini_key_value
  return 0
}