#! /bin/bash
#
# "THE BEER-WARE LICENSE" (Revision 42):
# <xxleite@gmail.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return
#

source util.sh

use_dir "compiler"

source base.sh

process "CC='gcc -fPIC'" "http://zlib.net/zlib-1.2.7.tar.gz" "--shared"
process "CC='gcc'" "http://tukaani.org/lzma/lzma-4.32.7.tar.gz" ""
process "CC='gcc'" "http://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.14.tar.gz" "--enable-static --enable-shared"
process "" "ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.32.tar.gz" ""
process "" "http://ufpr.dl.sourceforge.net/project/mcrypt/Libmcrypt/2.5.8/libmcrypt-2.5.8.tar.gz" "--enable-static --enable-shared"
process "" "ftp://xmlsoft.org/libxml2/libxml2-sources-2.9.0.tar.gz" ""
process "" "http://bzip.org/1.0.6/bzip2-1.0.6.tar.gz" "make -f Makefile-libbz2_so"

# remove packages to recompile them
eval "rm ${base_dir}libs/zlib-1.2.7 ${base_dir}libs/lzma-4.32.7 ${base_dir}libs/libiconv-1.14 ${base_dir}libs/pcre-8.32 ${base_dir}libs/libmcrypt-2.5.8 ${base_dir}libs/libxml2-sources-2.9.0 ${base_dir}libs/bzip2-1.0.6 -r -f"

process "CC='gcc'" "http://www.hpl.hp.com/personal/Hans_Boehm/gc/gc_source/gc.tar.gz" ""
process "" "ftp://sourceware.org/pub/libffi/libffi-3.0.13.tar.gz" ""
process "" "http://ftp.gnu.org/gnu/libunistring/libunistring-0.9.3.tar.gz" "--prefix=${base_dir}output"
process "" "ftp://ftp.gnu.org/gnu/libtool/libtool-2.4.tar.gz" "--enable-ltdl-install --prefix=${base_dir}output"
process "CC='gcc' CFLAGS='-O3 -w -pipe -arch x86_64' CXXFLAGS='-O3 -w -pipe -arch x86_64' LDFLAGS='-arch x86_64'" \
        "ftp://ftp.gnu.org/gnu/gmp/gmp-5.1.1.tar.bz2" "--build=x86_64-linux --enable-cxx --enable-shared --enable-static --prefix=${base_dir}output"

eval "\cp -r -f ${base_dir}output/* /usr/local"
source base.sh

#process "CFLAGS='' CXXFLAGS='' LDFLAGS=''" \
#        "ftp://ftp.gnu.org/gnu/guile/guile-2.0.7.tar.gz" \
#        "--with-libgmp-prefix=${base_dir}output --with-libunistring-prefix=${base_dir}output --with-libintl-prefix=${base_dir}output --with-libltdl-prefix=${base_dir}output --prefix=${base_dir}output"

process "" "http://gcc-uk.internet.bs/infrastructure/brik2.tar.gz" ""
process "./autogen.sh" "http://www.bastoul.net/cloog/pages/download/piplib-1.4.0.tar.gz" "--with-gmp=${base_dir}output --prefix=${base_dir}output"
process "" "http://gcc-uk.internet.bs/infrastructure/cctools-590.36.tar.bz2" ""
process "" "ftp://ftp.gnu.org/gnu/dejagnu/dejagnu-1.5.tar.gz" "--enable-shared --enable-static --prefix=${base_dir}output"
process "" "http://icps.u-strasbg.fr/polylib/polylib_src/polylib-5.22.5.tar.gz" "--with-libgmp=${base_dir}output --prefix=${base_dir}output"
process "" "http://bugseng.com/products/ppl/download/ftp/snapshots/ppl-1.1pre7.tar.gz" \
  "--enable-shared --enable-static --with-gmp=${base_dir}output --prefix=${base_dir}output"
process "" "http://gcc-uk.internet.bs/infrastructure/isl-0.11.1.tar.bz2" \
  "--enable-shared --enable-static --with-gmp-prefix=${base_dir}output --with-piplib-prefix=${base_dir}output --prefix=${base_dir}output"
process "" "http://gcc-uk.internet.bs/infrastructure/cloog-0.18.0.tar.gz" \
  "--enable-shared --enable-static --with-gmp-prefix=${base_dir}output --with-isl-prefix=${base_dir}output --with-bits=gmp --prefix=${base_dir}output"
process "" "http://gcc-uk.internet.bs/infrastructure/cloog-parma-0.16.1.tar.gz" \
  "--with-gmp-prefix=${base_dir}output --with-ppl-prefix=${base_dir}output --with-cloog-prefix=${base_dir}output --prefix=${base_dir}output"
process "" "http://www.mpfr.org/mpfr-current/mpfr-3.1.2.tar.bz2" "--enable-shared --enable-static --prefix=${base_dir}output --with-gmp=${base_dir}output"
process "" "http://www.multiprecision.org/mpc/download/mpc-1.0.1.tar.gz" "--enable-shared --enable-static --prefix=${base_dir}output --with-gmp=${base_dir}output --with-mpfr=${base_dir}output"
process "" "http://www.mr511.de/software/libelf-0.8.13.tar.gz" "--enable-shared --enable-static --prefix=${base_dir}output"
process "export POOMASUITE=LINUXgcc" "http://download.savannah.gnu.org/releases/freepooma/freepooma-2.4.1.tar.gz" "--prefix ${base_dir}output --arch LINUXgcc"

eval "\cp -r -f ${base_dir}output/* /usr/local"

process "export LD_LIBRARY_PATH=/opt/gcc/output/lib:$LD_LIBRARY_PATH" "http://gcc-uk.internet.bs/releases/gcc-4.8.0/gcc-4.8.0.tar.bz2" \
  "--disable-bootstrap --disable-libstdcxx-pch --enable-languages=c,c++,go,objc --enable-libgomp --enable-lto --enable-threads=posix --enable-tls --with-gmp=${base_dir}output --with-mpfr=${base_dir}output --with-mpc=${base_dir}output --with-libelf=${base_dir}output --with-fpmath=sse --enable-cloog-backend=isl --disable-multilib --build=x86_64-linux-gnu --enable-checking=release --enable-clocale=gnu --enable-gnu-unique-object --enable-nis --enable-objc-gc --enable-shared --host=x86_64-linux-gnu --target=x86_64-linux-gnu --with-arch-32=i686 --with-tune=generic --with-low-memory -v" #\
  "make -j8 > /dev/null && make install > /dev/null && make clean > /dev/null"