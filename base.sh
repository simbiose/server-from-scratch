#! /bin/bash
#
# "THE BEER-WARE LICENSE" (Revision 42):
# <xxleite@gmail.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return
#

export CC="gcc"
export CHOST="x86_64-linux-gnu"
export PATH="${base_dir}output/bin:$PATH"
#export CFLAGS="-O2 -pipe -ggdb -march=native -msahf -mcx16 -fgcse-sm -fgcse-las -fgcse-after-reload -ftracer -floop-interchange -floop-strip-mine -floop-block -floop-parallelize-all -ftree-parallelize-loops=2"
#export CXXFLAGS="${CFLAGS}" 
#export LDFLAGS="-Wl,-O1 -Wl,--as-needed -Wl,--add-needed -Wl,--hash-style=both -Wl,--sort-common -fopenmp -lgomp" 
#export MAKEOPTS="-j4" 
export I_PROMISE_TO_SUPPLY_PATCHES_WITH_BUGS=1



