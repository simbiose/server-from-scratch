#! /bin/bash
:<<LICENSE
  "THE BEER-WARE LICENSE" (Revision 42):
  <xxleite@gmail.com> wrote this file. As long as you retain this notice you
  can do whatever you want with this stuff. If we meet some day, and you think
  this stuff is worth it, you can buy me a beer in return
LICENSE

cat > /etc/apt/sources.list <<"EOF"
deb ftp://ftp.us.debian.org/debian/ squeeze main contrib non-free
deb-src ftp://ftp.us.debian.org/debian/ squeeze main contrib non-free

deb http://security.debian.org/ squeeze/updates main
EOF

apt-get update
apt-get upgrade
apt-get install build-essential libmysql++-dev libmysqlclient-dev gettext automake autoconf autogen libjpeg62-dev libfreetype6-dev libxpm-dev libgmp3-dev libmpfr-dev libc-client2007e-dev libmcrypt-dev libpspell-dev libcurl4-gnutls-dev libgdbm-dev libdb4.8 libdb4.8-dev bzr cmake chkconfig bison libaio-dev libaio1 libpaper-dev libpaper-utils libglu1-mesa-dev freeglut3-dev mesa-common-dev figlet gperf flex expect xutils-dev

if [ $? -gt 0 ]; then
  err "build essential error!"
  exit 1
fi

clear

source util.sh

./gcc.sh

assert "fail to compile gcc" ""

use_dir "."

write_motd "regulus" "cola"

source base.sh

process "CC='gcc -fPIC'" "http://zlib.net/zlib-1.2.7.tar.gz" "--shared"
process "CC='gcc'" "http://tukaani.org/lzma/lzma-4.32.7.tar.gz" ""
process "" "ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.32.tar.gz" ""
process "" "http://ufpr.dl.sourceforge.net/project/mcrypt/Libmcrypt/2.5.8/libmcrypt-2.5.8.tar.gz" "--enable-static --enable-shared"
process "" "ftp://xmlsoft.org/libxml2/libxml2-sources-2.9.0.tar.gz" ""
process "" "http://bzip.org/1.0.6/bzip2-1.0.6.tar.gz" "make -f Makefile-libbz2_so"
process "" "http://superb-dca3.dl.sourceforge.net/project/libpng/libpng15/1.5.14/libpng-1.5.14.tar.gz" ""
process "" "http://www.ijg.org/files/jpegsrc.v8d.tar.gz" "--enable-shared"
process "" "ftp://invisible-island.net/ncurses/current/ncurses-5.9-20130324.tgz" "--with-shared"
process "" "ftp://ftp.cwru.edu/pub/bash/readline-6.2.tar.gz" ""
process "" "http://www.openssl.org/source/openssl-0.9.8x.tar.gz" "shared"
process "" "http://www.pdflib.com/binaries/PDFlib/705/PDFlib-Lite-7.0.5p3.tar.gz" ""
process "" "http://pyyaml.org/download/libyaml/yaml-0.1.4.tar.gz" "--prefix=/usr/local"
process "" "http://www.sqlite.org/sqlite-autoconf-3071502.tar.gz" ""
process "" "http://savannah.c3sl.ufpr.br/freetype/freetype-2.4.11.tar.gz" ""
process "" "https://click-and-run.googlecode.com/files/gd-2.0.35.tar.gz" ""
process "" "ftp://ftp.unixodbc.org/pub/unixODBC/unixODBC-2.3.1.tar.gz" "--enable-static"
process "" "http://git-core.googlecode.com/files/git-1.8.1.tar.gz" ""

puts ""
puts "creating users and groups for nginx, sphinx and percona (mysql)"

./useradd.sh "nginx" "www-data" 1

if [ $? -gt 0 ]; then err "nginx user/group creation fail"; exit 1; fi

./useradd.sh "mysql" "mysql" 1

if [ $? -gt 0 ]; then err "mysql user/group creation fail"; exit 1; fi

./useradd.sh "sphinx" "sphinx" 1

if [ $? -gt 0 ]; then err "sphinx user/group creation fail"; exit 1; fi

puts ""
puts "pre-configuring percona"
download "http://sphinxsearch.com/files/sphinx-2.0.7-release.tar.gz"
download "http://www.percona.com/redir/downloads/Percona-Server-5.5/LATEST/source/Percona-Server-5.5.30-rel30.1.tar.gz"

if [[ "http://www.percona.com/redir/downloads/Percona-Server-5.5/LATEST/source/Percona-Server-5.5.30-rel30.1.tar.gz" =~ $regex ]]; then
  mysql_folder="${BASH_REMATCH[2]}"
else
  err "... fail to get percona folder ..."
  exit 1
fi

mkdir -p /etc/mysql
mkdir -p /etc/mysql/conf.d
mkdir -p /var/lib/mysql
mkdir -p /var/lib/mysql/data

mkdir -p /etc/sphinx

mkdir -p /etc/nginx
mkdir -p /etc/nginx/sites-enabled

puts ""
puts "sphinx as percona plugin"
cd libs/"$mysql_folder"
cp -R ../sphinx-*/mysqlse/ storage/sphinx
cd ../..

if [ $? -gt 0 ]; then err "sphinx plugin fail"; exit 1; fi

# nginx, 
process "" "http://nginx.org/download/nginx-1.2.6.tar.gz" "--with-openssl=../libs/openssl-0.9.8x/ --with-http_ssl_module --with-pcre=../libs/pcre-8.32 --user=nginx --group=www-data"
process "" "http://sphinxsearch.com/files/sphinx-2.0.7-release.tar.gz" "--prefix=/usr/local/sphinx"
process "" "http://www.percona.com/redir/downloads/Percona-Server-5.5/LATEST/source/Percona-Server-5.5.30-rel30.1.tar.gz" " . -DCMAKE_INSTALL_PREFIX=/usr/bin/mysql -DINSTALL_MYSQLDATADIR=/var/lib/mysql/data -DMYSQL_DATADIR=/var/lib/mysql/data -DSYSCONFDIR=/etc/mysql -DWITH_READLINE=ON -DWITH_WSREP=OFF -DENABLED_PROFILING=OFF -DMYSQL_MAINTAINER_MODE=OFF -DWITH_DEBUG=OFF -DBUILD_CONFIG=mysql_release -DIGNORE_AIO_CHECK=true -DWITH_SPHINX_STORAGE_ENGINE=1 -DWITH_EMBEDDED_SERVER=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo"

puts "setting nginx service up and more ..."

ln -s /usr/local/nginx/sbin/nginx /usr/sbin/nginx
cp confs/nginx.script /etc/init.d/nginx
cp confs/nginx.default /etc/nginx/nginx.conf
cp confs/fastcgi.default /etc/nginx/fastcgi.conf
cp confs/mime.default /etc/nginx/mime.conf
chmod +x /etc/init.d/nginx
mkdir -p /var/log/nginx
touch /var/log/nginx/access.log
touch /var/log/nginx/error.log
chmod 640 /var/log/nginx/access.log
chmod 640 /var/log/nginx/error.log
chown nginx:www-data /var/log/nginx/access.log
chown nginx:www-data /var/log/nginx/error.log
/usr/sbin/update-rc.d -f nginx defaults

if [ $? -gt 0 ]; then err "nginx setup fail"; exit 1; fi

puts "setting sphinx service up and more ..."

cp confs/sphinx.default /etc/sphinx/sphinx.conf
cp confs/sphinx.script /etc/init.d/sphinx
chmod +x /etc/init.d/sphinx
/usr/sbin/update-rc.d -f sphinx defaults

if [ $? -gt 0 ]; then err "sphinx setup fail"; exit 1; fi

puts "setting mysql service up and more ... "

chown -R mysql:mysql /etc/mysql
chown -R mysql:mysql /etc/mysql/conf.d
chown -R mysql:mysql /var/lib/mysq

# sudo vi /etc/ssh/sshd_config
## KeepAlive yes 
## ClientAliveInterval 120 
## ClientAliveCountMax 5


process "" "http://git-core.googlecode.com/files/git-1.8.1.tar.gz" ""
process "" "http://superb-dca3.dl.sourceforge.net/project/expat/expat/2.1.0/expat-2.1.0.tar.gz" ""
process "" "http://superb-dca3.dl.sourceforge.net/project/libjpeg/libjpeg/6b/jpegsrc.v6b.tar.gz" ""
process "" "http://tukaani.org/xz/xz-5.0.4.tar.gz" "--with-libiconv"
process "" "ftp://ftp.remotesensing.org/libtiff/tiff-4.0.3.tar.gz" ""
process "" "https://webp.googlecode.com/files/libwebp-0.2.1.tar.gz" ""
process "" "http://superb-dca2.dl.sourceforge.net/project/wvware/libwmf/0.2.8.4/libwmf-0.2.8.4.tar.gz" "--with-libxml2 --with-freetype --with-zlib --with-png --with-jpeg"
process "" "http://www.freedesktop.org/software/fontconfig/release/fontconfig-2.10.92.tar.gz" "--with-libiconv --enable-libxml2 --enable-iconv"
process "" "http://downloads.ghostscript.com/public/ghostscript-9.07.tar.gz" "--with-libiconv"

# shame on ruby ... what tha fuck needs ruby to compile ruby???
process "" "ftp://ftp.ruby-lang.org/pub/ruby/1.8/ruby-1.8.7-p370.tar.gz" ""
process "" "ftp://ftp.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p0.tar.gz" "--disable-install-doc --with-opt-dir=/usr/local/lib"
process "" "http://luajit.org/download/LuaJIT-2.0.1.tar.gz" ""


process "" "https://click-and-run.googlecode.com/files/gd-2.0.35.tar.gz" ""
process "" "http://www.graphviz.org/pub/graphviz/stable/SOURCES/graphviz-2.30.1.tar.gz" "--with-libgd --with-webp --with-expat"


# img magick necessary libs


#download "ftp://ftp.fifi.org/pub/ImageMagick/ImageMagick.tar.gz"
#process "" "http://www.magickwand.org/download/php/MagickWandForPHP-1.0.9-2.tar.gz" ""
#download "http://us3.php.net/distributions/php-5.4.13.tar.gz"
#process "" "" "--enable-bcmath --enable-calendar --enable-exif --enable-ftp --enable-gd-native-ttf --enable-mbstring --enable-shmop --enable-sockets --enable-sysvsem --enable-sysvshm --enable-tokenizer --enable-wddx --with-openssl --with-zlib-dir --with-zlib --with-bz2 --with-jpeg-dir --with-curl --with-db4 --with-gd --with-png-dir --with-xpm-dir --with-freetype-dir --with-gettext --with-gmp --with-imap --with-imap-ssl --with-ldap --with-mcrypt=/usr/local/libmcrypt --with-mhash --with-mysql --with-pspell --with-regex=php --with-xmlrpc --with-iconv --with-tsrm-st --with-tsrm-pthreads --with-kerberos"

#tar xzvf ruby-1.9.3-p0.tar.gz
#cd ruby-1.9.3-p0
#./configure --prefix=/usr/local --enable-shared --disable-install-doc --with-opt-dir=/usr/local/lib
#make
#make install

