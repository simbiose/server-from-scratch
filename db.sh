#!/bin/bash
#
# "THE BEER-WARE LICENSE" (Revision 42):
# <xxleite@gmail.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return
#

source util.sh

use_dir "."

percona_src_url="http://www.percona.com/redir/downloads/Percona-Server-5.5/LATEST/source/Percona-Server-5.5.30-rel30.1.tar.gz"
sphinx_src_url="http://sphinxsearch.com/files/sphinx-2.0.7-release.tar.gz"

puts ""
puts "creating users and groups for sphinx and percona (mysql)"

user_add "mysql" "mysql"
assert "mysql user/group creation fail"

user_add "sphinx" "sphinx"
assert "sphinx user/group creation fail"

puts ""
puts "pre-configuring percona"
download $sphinx_src_url
download $percona_src_url

if [[ $percona_src_url =~ $regex ]]; then
  mysql_folder="${BASH_REMATCH[2]}"
else
  assert "... fail to get percona folder ..."
fi

mkdir -p /etc/sphinx
mkdir -p /etc/mysql
mkdir -p /etc/mysql/conf.d
mkdir -p /var/lib/mysql
mkdir -p /var/lib/mysql/data

puts ""
puts "sphinx as percona plugin"
cd "${base_dir}"libs/"${mysql_folder}"
cp -R "${base_dir}"/sphinx-*/mysqlse/ storage/sphinx
cd "${base_dir}"

assert "sphinx plugin fail"

process "" $sphinx_src_url "--prefix=/usr/local/sphinx"
process "" $percona_src_url \
  " . -DCMAKE_INSTALL_PREFIX=/usr/bin/mysql -DINSTALL_MYSQLDATADIR=/var/lib/mysql/data -DMYSQL_DATADIR=/var/lib/mysql/data -DSYSCONFDIR=/etc/mysql -DWITH_READLINE=ON -DWITH_WSREP=OFF -DENABLED_PROFILING=OFF -DMYSQL_MAINTAINER_MODE=OFF -DWITH_DEBUG=OFF -DBUILD_CONFIG=mysql_release -DIGNORE_AIO_CHECK=true -DWITH_SPHINX_STORAGE_ENGINE=1 -DWITH_EMBEDDED_SERVER=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo"

assert "sphinx/percona setup fail"

puts "setting sphinx service up and more ..."

cp confs/sphinx.default /etc/sphinx/sphinx.conf
cp confs/sphinx.script /etc/init.d/sphinx
chmod +x /etc/init.d/sphinx
/usr/sbin/update-rc.d -f sphinx defaults

assert "sphinx setup fail"

puts "setting mysql service up and more ... "

chown -R mysql:mysql /etc/mysql
chown -R mysql:mysql /etc/mysql/conf.d
chown -R mysql:mysql /var/lib/mysql


